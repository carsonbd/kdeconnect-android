package org.kde.kdeconnect.Plugins.AirMousePlugin;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.preference.Preference;
import androidx.preference.PreferenceScreen;
import androidx.preference.SwitchPreference;

import org.kde.kdeconnect.UserInterface.PluginSettingsFragment;
import org.kde.kdeconnect_tp.R;

public class ShareSettingsFragment extends PluginSettingsFragment {
    private Preference holdPreference;

    public static ShareSettingsFragment newInstance(@NonNull String pluginKey) {
        ShareSettingsFragment fragment = new ShareSettingsFragment();
        fragment.setArguments(pluginKey);

        return fragment;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);

        PreferenceScreen preferenceScreen = getPreferenceScreen();
        final SwitchPreference alwaysOnPreference = preferenceScreen.findPreference(getString(R.string.airmouse_always_on_key));
        holdPreference = preferenceScreen.findPreference(getString(R.string.airmouse_hold_key));

        if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)) {
            updateHoldListEnable(alwaysOnPreference.isChecked());
            alwaysOnPreference.setOnPreferenceChangeListener((preference, newValue) -> {
                updateHoldListEnable((Boolean) newValue);
                return true;
            });
        } else {
            updateHoldListEnable(alwaysOnPreference.isChecked());
        }
    }

    private void updateHoldListEnable(Boolean enable) {
        holdPreference.setEnabled(enable);
    }
}
