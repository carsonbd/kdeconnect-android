/*
 * SPDX-FileCopyrightText: 2022 Albert Manakhov <armanakhov@ya.ru>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

package org.kde.kdeconnect.Plugins.AirMousePlugin;

import android.view.MotionEvent;
import android.view.ViewConfiguration;

class AirMouseGestureDetector {

    private static final int TAP_TIMEOUT = ViewConfiguration.getTapTimeout() + 100;
    private final OnTapListener mTapListener;

    private long mFirstDownTime = 0;

    public interface OnTapListener {

        boolean onHold(MotionEvent ev);

        boolean onRelease(MotionEvent ev);
    }

    AirMouseGestureDetector(OnTapListener gestureListener) {
        if (gestureListener == null) {
            throw new IllegalArgumentException("gestureListener cannot be null");
        }
        mTapListener = gestureListener;
    }

    boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        boolean mIsTapHandled = false;
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mFirstDownTime = event.getEventTime();
                break;
            case MotionEvent.ACTION_MOVE:
                int count = event.getPointerCount();
                if (count == 1 && event.getEventTime() - mFirstDownTime >= TAP_TIMEOUT) {
                    mIsTapHandled = mTapListener.onHold(event);
                }
                break;
            case MotionEvent.ACTION_UP:
                if (event.getEventTime() - mFirstDownTime >= TAP_TIMEOUT) {
                    mIsTapHandled = mTapListener.onRelease(event);
                }
                mFirstDownTime = 0;
                break;
        }
        return mIsTapHandled;
    }
}
