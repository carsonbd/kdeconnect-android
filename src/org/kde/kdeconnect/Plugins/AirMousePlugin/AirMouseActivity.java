/*
 * SPDX-FileCopyrightText: 2022 Albert Manakhov <armanakhov@ya.ru>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

package org.kde.kdeconnect.Plugins.AirMousePlugin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.GestureDetector;
import android.view.HapticFeedbackConstants;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import org.kde.kdeconnect.BackgroundService;
import org.kde.kdeconnect.Plugins.MousePadPlugin.ComposeSendActivity;
import org.kde.kdeconnect.Plugins.MousePadPlugin.KeyListenerView;
import org.kde.kdeconnect.Plugins.MousePadPlugin.MousePadPlugin;
import org.kde.kdeconnect.UserInterface.ThemeUtil;
import org.kde.kdeconnect_tp.R;
import org.kde.kdeconnect_tp.databinding.ActivityAirMouseBinding;

import java.util.Objects;

public class AirMouseActivity extends AppCompatActivity implements SensorEventListener, GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener, AirMouseGestureDetector.OnTapListener {
    private String deviceId;

    private final static float MinDistanceToSendGenericScroll = 0.1f; // real mouse scroll wheel event
    private final static float MinDistanceToSendScroll = 2.5f; // touch gesture scroll
    private final static float MinDistanceToSendGyroscope = 0.005f; // gyroscope minimum position change
    private boolean isGyroscopeRegistered = false;
    private boolean isDragNDrop = false;

    private float mCurrentSensitivity;

    private SensorManager sensorManager;
    private ActivityAirMouseBinding binding;
    private GestureDetector mDetector;
    private AirMouseGestureDetector airMouseGestureDetector;
    private KeyListenerView keyListenerView;

    private float accumulatedDistanceY = 0;
    private boolean alwaysOn;
    private Runnable onHoldAction;
    private Runnable onReleaseAction;
    private Runnable onSingleTapAction;
    private Runnable onDoubleTapAction;
    private int scrollDirection = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeUtil.setUserPreferredTheme(this);

        binding = ActivityAirMouseBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(findViewById(R.id.toolbar));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        deviceId = getIntent().getStringExtra("deviceId");

        keyListenerView = findViewById(R.id.keyListener);
        keyListenerView.setDeviceId(deviceId);

        getWindow().getDecorView().setHapticFeedbackEnabled(true);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);


        mDetector = new GestureDetector(this, this);
        airMouseGestureDetector = new AirMouseGestureDetector(this);
        mDetector.setOnDoubleTapListener(this);

        setPreferences(prefs);

        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(visibility -> {
            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {

                int fullscreenType = 0;

                fullscreenType |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    fullscreenType |= View.SYSTEM_UI_FLAG_FULLSCREEN;
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    fullscreenType |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                }

                getWindow().getDecorView().setSystemUiVisibility(fullscreenType);
            }
        });

    }

    private void setPreferences(SharedPreferences prefs) {
        setSensitivityPreferences(prefs);
        setButtonsPreferences(prefs);
        setAlwaysOnPreference(prefs);
        setOnHold(prefs);
        setOnSingleTap(prefs);
        setOnDoubleTap(prefs);
        setScrollDirection(prefs);
    }

    private void setScrollDirection(SharedPreferences prefs) {
        if (prefs.getBoolean(getString(R.string.airmouse_scroll_direction_key),
                getResources().getBoolean(R.bool.airmouse_default_scroll_direction))) {
            scrollDirection = -1;
        } else {
            scrollDirection = 1;
        }
    }


    private void setOnDoubleTap(SharedPreferences prefs) {
        String onSingleTap = prefs.getString(getString(R.string.airmouse_double_key),
                getString(R.string.airmouse_default_double));
        switch (onSingleTap) {
            case "left":
                onDoubleTapAction = this::sendLeftClick;
                break;
            case "right":
                onDoubleTapAction = this::sendRightClick;
                break;
            case "middle":
                onDoubleTapAction = this::sendMiddleClick;
                break;
            case "drag_n_drop":
                onDoubleTapAction = this::enableDragNDrop;
                break;
            case "none":
                onDoubleTapAction = () -> {
                };

        }
    }

    private void setOnSingleTap(SharedPreferences prefs) {
        String onSingleTap = prefs.getString(getString(R.string.airmouse_single_key),
                getString(R.string.airmouse_default_single));
        switch (onSingleTap) {
            case "left":
                onSingleTapAction = this::sendLeftClick;
                break;
            case "right":
                onSingleTapAction = this::sendRightClick;
                break;
            case "middle":
                onSingleTapAction = this::sendMiddleClick;
                break;
            case "drag_n_drop":
                onSingleTapAction = this::enableDragNDrop;
                break;
            case "none":
                onSingleTapAction = () -> {
                };

        }
    }

    private void setOnHold(SharedPreferences prefs) {
        if (alwaysOn) {
            String onHoldSetting = prefs.getString(getString(R.string.airmouse_hold_key),
                    getString(R.string.airmouse_default_hold));
            switch (onHoldSetting) {
                case "disable_mouse":
                    onHoldAction = this::unregisterGyroscopeSensorListener;
                    onReleaseAction = this::registerGyroscopeSensorListener;
                    break;
                case "drag_n_drop":
                    onHoldAction = this::enableDragNDrop;
                    onReleaseAction = this::disableDragNDrop;
                    break;
                case "none":
                    onHoldAction = () -> {
                    };
                    onReleaseAction = () -> {
                    };
                    break;
            }
        }
    }

    private void setSensitivityPreferences(SharedPreferences prefs) {
        String sensitivitySetting = prefs.getString(getString(R.string.airmouse_sensitivity_key),
                getString(R.string.airmouse_default_sensitivity));
        switch (sensitivitySetting) {
            case "slowest":
                mCurrentSensitivity = 5f;
                break;
            case "aboveSlowest":
                mCurrentSensitivity = 12f;
                break;
            case "aboveDefault":
                mCurrentSensitivity = 36f;
                break;
            case "fastest":
                mCurrentSensitivity = 48f;
                break;
            case "default":
            default:
                mCurrentSensitivity = 24f;
                break;
        }
    }

    private void setButtonsPreferences(SharedPreferences prefs) {
        boolean buttonVisability = prefs.getBoolean(getString(R.string.airmouse_enable_buttons_key),
                getResources().getBoolean(R.bool.airmouse_default_enable_buttons));
        if (buttonVisability) {
            binding.mouseLeft.setVisibility(View.VISIBLE);
            binding.mouseMiddle.setVisibility(View.VISIBLE);
            binding.mouseRight.setVisibility(View.VISIBLE);
            BackgroundService.RunWithPlugin(this, deviceId, AirMousePlugin.class, plugin -> runOnUiThread(() -> {
                binding.mouseLeft.setOnClickListener(v -> sendLeftClick());
                binding.mouseMiddle.setOnClickListener(v -> sendMiddleClick());
                binding.mouseRight.setOnClickListener(v -> sendRightClick());
            }));
        }
    }


    private void setAlwaysOnPreference(SharedPreferences prefs) {
        alwaysOn = prefs.getBoolean(getString(R.string.airmouse_always_on_key),
                getResources().getBoolean(R.bool.airmouse_default_always_on));

    }

    @Override
    public void onStart() {
        super.onStart();
        if (alwaysOn) {
            registerGyroscopeSensorListener();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterGyroscopeSensorListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_airmouse, menu);

        BackgroundService.RunWithPlugin(this, deviceId, AirMousePlugin.class, plugin -> {
            if (!plugin.isKeyboardEnabled()) {
                menu.removeItem(R.id.menu_show_keyboard);
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_show_keyboard) {
            showKeyboard();
            return true;
        } else if (id == R.id.menu_open_compose_send) {
            Intent intent = new Intent(this, ComposeSendActivity.class);
            intent.putExtra("org.kde.kdeconnect.Plugins.MousePadPlugin.deviceId", deviceId);
            startActivity(intent);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public void gyroscopeEvent(SensorEvent event) {
        BackgroundService.RunWithPlugin(this, deviceId, MousePadPlugin.class, plugin -> {
            float rawX = -event.values[2];
            float rawY = -event.values[0];
            float xDelta = rawX > MinDistanceToSendGyroscope || rawX < -MinDistanceToSendGyroscope //check needed for drifting correction
                    ? rawX * mCurrentSensitivity
                    : 0;
            float yDelta = rawY > MinDistanceToSendGyroscope || rawY < -MinDistanceToSendGyroscope
                    ? rawY - event.values[0] * mCurrentSensitivity
                    : 0;
            if (xDelta != 0 || yDelta != 0) {
                plugin.sendMouseDelta(xDelta, yDelta);
            }
        });
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            gyroscopeEvent(event);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        //Ignored
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (airMouseGestureDetector.onTouchEvent(event)) {
            return true;
        }
        return mDetector.onTouchEvent(event);
    }

    //TODO: Does not work on KitKat with or without requestFocus()
    private void showKeyboard() {
        InputMethodManager imm = ContextCompat.getSystemService(this, InputMethodManager.class);
        keyListenerView.requestFocus();
        imm.toggleSoftInputFromWindow(keyListenerView.getWindowToken(), 0, 0);
    }


    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return true;
    }

    @Override
    public boolean onHold(MotionEvent ev) {
        if (alwaysOn) {
            onHoldAction.run();
        } else {
            registerGyroscopeSensorListener();
        }
        return true;
    }

    @Override
    public boolean onRelease(MotionEvent ev) {
        if (alwaysOn) {
            onReleaseAction.run();
        } else {
            unregisterGyroscopeSensorListener();
            disableDragNDrop();
        }
        return true;
    }

    private void registerGyroscopeSensorListener() {
        if (!isGyroscopeRegistered) {
            if (sensorManager == null) {
                sensorManager = ContextCompat.getSystemService(this, SensorManager.class);
            }
            sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_GAME);
            isGyroscopeRegistered = true;
        }
    }

    private void unregisterGyroscopeSensorListener() {
        if (isGyroscopeRegistered) {
            if (sensorManager != null) {
                sensorManager.unregisterListener(this);
            }
            disableDragNDrop();
            isGyroscopeRegistered = false;
        }
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        onSingleTapAction.run();
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent motionEvent) {
        onDoubleTapAction.run();
        return true;
    }

    private void enableDragNDrop() {
        if (!isDragNDrop) {
            sendHoldSignal();
            registerGyroscopeSensorListener();
        }
        isDragNDrop = true;
    }

    private void disableDragNDrop() {
        if (isDragNDrop) {
            sendLeftClick();
            isDragNDrop = false;
        }
    }


    private void sendHoldSignal() {
        getWindow().getDecorView().performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        System.out.println("dnd");
        BackgroundService.RunWithPlugin(this, deviceId, MousePadPlugin.class, MousePadPlugin::sendSingleHold);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent e) {
        if (e.getAction() == MotionEvent.ACTION_SCROLL) {
            final float distanceY = e.getAxisValue(MotionEvent.AXIS_VSCROLL);

            accumulatedDistanceY += distanceY;

            if (accumulatedDistanceY > MinDistanceToSendGenericScroll || accumulatedDistanceY < -MinDistanceToSendGenericScroll) {
                accumulatedDistanceY = 0;
            }

        }

        return super.onGenericMotionEvent(e);
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, final float distanceX, final float distanceY) {
        // If only one thumb is used then cancel the scroll gesture
        if (e2.getPointerCount() <= 1) {
            return false;
        }

        accumulatedDistanceY += distanceY;

        if (accumulatedDistanceY > MinDistanceToSendScroll || accumulatedDistanceY < -MinDistanceToSendScroll) {
            sendScroll(scrollDirection * accumulatedDistanceY);
            accumulatedDistanceY = 0;
        }

        return true;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }


    private void sendLeftClick() {
        BackgroundService.RunWithPlugin(this, deviceId, MousePadPlugin.class, MousePadPlugin::sendLeftClick);
    }

    private void sendMiddleClick() {
        BackgroundService.RunWithPlugin(this, deviceId, MousePadPlugin.class, MousePadPlugin::sendMiddleClick);
    }

    private void sendRightClick() {
        BackgroundService.RunWithPlugin(this, deviceId, MousePadPlugin.class, MousePadPlugin::sendRightClick);
    }

    private void sendScroll(float y) {
        BackgroundService.RunWithPlugin(this, deviceId, MousePadPlugin.class, plugin -> plugin.sendScroll(0, y));
    }
}
